import { Container } from 'react-bootstrap';
import {Fragment, useState, useEffect} from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Swal from 'sweetalert2';
import './App.css';

// Components
import AppNavBar from './components/AppNavBar';
import Banner from './components/Banner';
import Footer from './components/Footer';

// Customer
import Home from './customer/Home';
import Login from './customer/Login';
import Register from './customer/Register';
import ProductCatalog from './customer/ProductCatalog';
import ProductView from './customer/ProductView';
import Cart from './customer/Cart';
import OrderHistory from './customer/OrderHistory';
import Logout from './customer/Logout';
import Error from './customer/Error';

// Admin
import CreateProduct from './admin/CreateProduct';
import RetrieveAllProducts from './admin/RetrieveAllProducts';
import Customers from './admin/Customers';
import Orders from './admin/Orders';


// UserContext
import { UserProvider } from './UserContext';




export default function App() {


// initialized as an object with properties from the local storage.
const [user, setUser] = useState({
  id: null,
  firstName: null,
  isAdmin: null,
  cart: null
})


// Function for clearing the localStorage on logout
const unsetUser = () => {
  localStorage.clear()
}



// Use fetch to connect to backend
useEffect(() => {
  fetch('https://ecommerce-bymagic.herokuapp.com/marketplace/users/details', {
    headers: {
      Authorization: `Bearer ${localStorage.getItem('token')}`
    }
  })
  .then(res => res.json())
  .then(data => {

    if(typeof data._id !== "undefined") {
      setUser({
        id: data._id,
        firstName: data.firstName,
        isAdmin: data.isAdmin,
        cart: data.cart
      })
    } else {
      setUser({
        id: null,
        firstName: null,
        isAdmin: null,
        cart: null

      })
    }
  })

}, [])


// fetch the product data for the cart
const [product, setProduct] = useState([])
const [cart, setCart] = useState([])

useEffect(() => {
  fetch('https://ecommerce-bymagic.herokuapp.com/marketplace/products/active')
  .then(res => res.json())
  .then(data => {

    setProduct(data)

  })
}, [])


const onAdd = (product) => {
    const exist = cart.find((x) => x._id === product._id);
    if (exist) {
      setCart(
        cart.map((x) =>
          x._id === product._id ? { ...exist, qty: exist.qty + 1 } : x
        )
      );
    } else {
      setCart([...cart, { ...product, qty: 1 }]);
    }
  };


const onRemove = (product) => {
  const exist = cart.find((x) => x._id === product._id);
  if(exist.qty === 1) {
      setCart(cart.filter((x) => x._id !== product._id))
    } else {
      setCart(
        cart.map((x) =>
          x.id === product.id ? { ...exist, qty: exist.qty - 1 } : x
        )
    )}
};


const removeFromCart = (product) => {
  setCart(cart.filter((x) => x._id !== product._id))
}; 


const removeAllCart = () => {
  setCart([])
};


// Archive a product
const archive = (productId) => {
  fetch(`https://ecommerce-bymagic.herokuapp.com/marketplace/products/archive/${productId}`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      "Authorization": `Bearer ${localStorage.getItem("token")}`
    }
  })
  .then(res => res.json())
  .then(data => {
    if(data === true) {
      Swal.fire({
        title: "Product Deactivated",
        icon: "success",
        text: "You have successfully deactivated a product!"
      })
    } else {
      Swal.fire({
        title: "Something went wrong",
        icon: "error",
        text: "Please try again"
      })
    }
  })
}



// Activate a product
const activate = (productId) => {
  fetch(`https://ecommerce-bymagic.herokuapp.com/marketplace/products/activate/${productId}`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      "Authorization": `Bearer ${localStorage.getItem("token")}`
    }
  })
  .then(res => res.json())
  .then(data => {
    if(data === true) {
      Swal.fire({
        title: "Product Activated",
        icon: "success",
        text: "You have successfully activate a product!"
      })
    } else {
      Swal.fire({
        title: "Something went wrong",
        icon: "error",
        text: "Please try again"
      })
    }
  })
}





  return (
    <UserProvider value= {{ user, 
                          setUser, 
                          unsetUser, 
                          onAdd, 
                          onRemove, 
                          cart, 
                          product, 
                          removeFromCart,
                          removeAllCart,
                          archive,
                          activate
                          }}>
      <Router>
        <Banner />
        <AppNavBar />
          <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/login" element={<Login />} />
          <Route path="/register" element={<Register />} />
          <Route path="/products" element={<ProductCatalog />} />
          <Route path="/products/:productId" element={<ProductView />}  />
          <Route path="/cart" element={<Cart />} />
          <Route path="/orderhistory" element={<OrderHistory />} />
          <Route path="/logout" element={<Logout />} />
          <Route path="/createproduct" element={<CreateProduct />} />
          <Route path="/allproducts" element={<RetrieveAllProducts />} />
          <Route path="/customers" element={<Customers />} />
          <Route path="/orders/:userId" element={<Orders />} />
          <Route path="*" element={<Error />} />
          </Routes>
        <Footer />
      </Router>
    </UserProvider>
    )
}

