import React from 'react';


// Create a userContext
const UserContext = React.createContext();


export const UserProvider = UserContext.Provider;



// Export the userContext
export default UserContext;