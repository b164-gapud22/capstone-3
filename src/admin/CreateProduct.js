
import { Container,Form, Button } from 'react-bootstrap';
import { useState, useContext } from 'react';
import UserContext from '../UserContext';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';



export default function CreateProduct() {

	const {user} = useContext(UserContext);

	const navigate =useNavigate();


	// State hooks to store the values of the inout fields
	const [productName, setProductName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	const [stock, setStock] = useState("");
	const [category, setCategory] = useState("");
	const [images, setImages] = useState("");





	// Handle upload

	const handleUpload = e => {


	try{

		const file = e.target.files[0]
		
		if(!file) return alert('File not exist')

		if(file.size > 3072 * 3072) return alert('File too large')


		let formData = new FormData()
		formData.append('file', file)



		fetch('https://ecommerce-bymagic.herokuapp.com/marketplace/upload', {
			method: "POST",
			headers: {
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: formData
		})
		.then(res => res.json())
		.then(data => {
			setImages(data)
			// console.log(data)
		})	


		} catch (err) {
			alert(err.response.data.msg)
		}

	}

	// Create a product
	function create(e) {

		// prevent automatic refresh
		e.preventDefault()

		fetch('https://ecommerce-bymagic.herokuapp.com/marketplace/products/addProduct', {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({

				productName: productName,
				description: description,
				price: price,
				stock: stock,
				category: category,
				images: images
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true) {

				setProductName("");
				setDescription("");
				setPrice("");
				setStock("");
				setCategory("");
				setImages("");

				Swal.fire({
					title: "Product Successfully Created",
					icon: "success",
				})
				navigate("/products")
			} else {
				Swal.fire({
					title: "Something went wrong!",
					icon: "error",
				})
			}

		})

	}






	return(
		<Container className="my-5 shadow p-4">	
			<h1 className="text-center">CREATE PRODUCT</h1>
			<Form onSubmit={e => {create(e)}}>
			  <Form.Group>
			    <Form.Label>Product Name:</Form.Label>
			    <Form.Control 
			    	type="text" 
			    	placeholder="Enter product name."
			    	value={productName}
			    	onChange={e => setProductName(e.target.value)}
			    	required
			    	 />
			  </Form.Group>

			  <Form.Group>
			    <Form.Label>Description:</Form.Label>
			    <Form.Control 
			    	type="text" 
			    	placeholder="Enter description." 
			    	value={description}
			    	onChange={e => setDescription(e.target.value)}
			    	required

			    	/>
			  </Form.Group>

			  <Form.Group>
			    <Form.Label>Price:</Form.Label>
			    <Form.Control 
			    	type="number" 
			    	placeholder="Enter price." 
			    	value={price}
			    	onChange={e => setPrice(e.target.value)}
			    	required
			    	/>
			  </Form.Group>

			  <Form.Group>
			    <Form.Label>Stocks:</Form.Label>
			    <Form.Control 
			    	type="number" 
			    	placeholder="Enter stocks."
			    	 value={stock}
			    	 onChange={e => setStock(e.target.value)}
			    	 required
			    	/>
			  </Form.Group>

			  <Form.Group>
			    <Form.Label>Category:</Form.Label>
			    <Form.Control 
			    	type="text" 
			    	placeholder="Enter category." 
			    	value={category}
			    	onChange={e => setCategory(e.target.value)}
			    	required
			    	/>
			  </Form.Group>

			  <div className="mb-3">
			   	<input type="file" name="file" onChange={handleUpload} className="col my-2" /> 
			   	<img src={images ? images.url: ''} width="150" alt="" />
			  </div>
			  
			  <Button variant="info" type="submit" >
			    Submit
			  </Button>
			</Form>
		</Container>
		);
};
