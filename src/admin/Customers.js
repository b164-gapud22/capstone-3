
import {Table, Container, Button} from 'react-bootstrap';
import { useEffect, useState } from 'react';
import {Link} from "react-router-dom";

export default function Customers() {

	const [user, setUser] = useState([])


		useEffect(() => {

			fetch('https://ecommerce-bymagic.herokuapp.com/marketplace/users/allUsers', {
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem("token")}`
				}
			})
			.then(res => res.json())
			.then(data => {
				setUser(data)
			})
		})




	return(

	<Container className="mb-5 mx-auto">
		<h3 className="text-center">CUSTOMER</h3>
		<Table responsive="sm" hover>
		  <thead>
		    <tr className="text-center">
		      <th>First Name</th>
		      <th>Last Name</th>
		      <th>Email</th>
		      <th>Mobile No.</th>
		      <th>Status</th>
		      <th>Orders</th>
		    </tr>
		  </thead>
		  <tbody>
		  {user.map(client => (
		  	<tr key={client._id}>
		  	  <td>{client.firstName}</td>
		  	  <td>{client.lastName}</td>
		  	  <td>{client.email}</td>
		  	  <td>{client.mobile}</td>
		  	  <td>{client.isAdmin ? <div>Admin</div> : <div>Customer</div>}</td>
		  	  <td>{client.isAdmin ? <div></div> : <Link to={`/orders/${client._id}`}>view</Link>}</td>
		  	</tr>
		  	))}		  	
		  </tbody>
		</Table>
	</Container>
	)
} 