import {Table, Container, Button} from 'react-bootstrap';
import {useState, useEffect} from 'react';
import {useParams} from 'react-router-dom';

export default function Orders() {

	const {userId} = useParams()


	const [orders, setOrders] = useState([])

	useEffect(() => {

		fetch(`https://ecommerce-bymagic.herokuapp.com/marketplace/users/orders/${userId}`, {
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setOrders(data.cart)
		})


	})
	console.log()
	return(
			<Container className="mb-5 mx-auto">
				<h3 className="text-center">CUSTOMER</h3>
				<Table responsive="sm" hover>
				  <thead>
				    <tr>
				      <th>Item</th>
				      <th>Name</th>
				      <th>Date</th>
				      <th>Price</th>
				      <th>Quantity</th>
				      <th>Total</th>
				    </tr>
				  </thead>
				  <tbody>
				  {orders.map(item => (

				  	<tr key={item._id}>
				  	  <td><img src={item.images[0].url} alt={orders.productName} width="100"/></td>
				  	  <td>{item.productName}</td>
				  	  <td>{item.orderOn}</td>
				  	  <td>&#8369;{(item.price).toLocaleString(undefined, {maximumFractionDigits: 2})}</td>
				  	  <td>{item.quantity}</td>
				  	  <td>&#8369;{(item.subtotal).toLocaleString(undefined, {maximumFractionDigits: 2})}</td>
				  	</tr>   

				  	))}	
				  </tbody>
				</Table>
			</Container>

		)
}