import {Fragment, useState, useEffect, useContext} from 'react';
import {Table, Container, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import UserContext from '../UserContext';



export default function RetrieveAllProducts() {


	// unpack the userContext
	const { archive, activate } = useContext(UserContext);

	const [allProducts, setAllProducts] = useState([])



	useEffect(() => {
		fetch('https://ecommerce-bymagic.herokuapp.com/marketplace/products/all')
		.then(res => res.json())
		.then(data => {

			setAllProducts(data)
		})
	})



	return (
			<Container className="mb-5 mx-auto">
				<h3 className="text-center">PRODUCTS</h3>
				<Table responsive="sm">
				  <thead>
				    <tr>
				      <th>Image</th>
				      <th>Name</th>
				      <th>Description</th>
				      <th>Price</th>
				      <th>Quantity</th>
				      <th>Status</th>
				    </tr>
				  </thead>
				  <tbody>
				  {allProducts.map(item => (

				  	<tr key={item._id}>
				  	  <td><img src={item.images.url} width="50" alt={item.productName}/></td>
				  	  <td>{item.productName}</td>
				  	  <td><small>{item.description}</small></td>
				  	  <td>&#8369;{(item.price).toLocaleString(undefined, {maximumFractionDigits: 2})}</td>
				  	  <td>{item.stock}pcs</td>
				  	  <td>{item.isActive ? <Button size="sm" className="bg-danger d-block" onClick={() => archive(item._id)}>Deactivate</Button> : <Button size="sm" className="bg-info block" onClick={() => activate(item._id)}> Activate </Button>}</td>
				  	  <td><Button as={Link} to={`/products/${item._id}`}>Edit</Button></td>
				  	</tr>
				  	))}
				  </tbody>
				</Table>
			</Container>

		)
}