import { Navbar, Container, Nav, NavDropdown, Form, FormControl, Button } from 'react-bootstrap';
import {Link} from 'react-router-dom';
import { useContext, useState, useEffect } from 'react';
import UserContext from '../UserContext';







export default function AppNavBar(props) {


	const {user, cart, product} = useContext(UserContext);

	// 
	const [firstName, setFirstName] = useState("")
	const [cartItems, setCartItems] = useState([])
	


	// 
	useEffect(() => {


		fetch('https://ecommerce-bymagic.herokuapp.com/marketplace/users/details', {
			headers: {
				Authorization: `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			
			setFirstName(data.firstName);
			setCartItems(data.cart);
			
		})
	}, [user])

		


	return(

		<Navbar bg="light" expand="lg" sticky="top">
		  <Navbar.Brand as={Link} to="/">
		  	<img
		  	        src="favicon-32x32.png"
		  	        width="30"
		  	        height="30"
		  	        className="d-inline-block align-top animate__animated animate__rotateIn"
		  	      />
		  </Navbar.Brand>
		  <Navbar.Brand as={Link} to="/">QuickShop</Navbar.Brand>
		  <Navbar.Toggle aria-controls="basic-navbar-nav" />
		  <Navbar.Collapse id="basic-navbar-nav">
		  	<Nav className="mr-auto">
		  		<Nav.Link className="mr-auto" as={Link} to="/" >Home</Nav.Link>
		  		{
		  			user.isAdmin ?
		  			<Nav.Link className="mr-auto" as={Link} to="/allproducts" >Products</Nav.Link>
		  			:
		  			<Nav.Link className="mr-auto" as={Link} to="/products" >Products</Nav.Link>
		  		}
		    	
		    </Nav>


		    {

		    	(user.id !== null) ?
		    	<Nav>
		    			
		    			{

		    				user.isAdmin ?
    						<NavDropdown title={`signed in as: ${firstName}`} id="collasible-nav-dropdown">
    				        <NavDropdown.Item as={Link} to="/createproduct">Create Product</NavDropdown.Item>
    				        <NavDropdown.Item as={Link} to="/customers">Customer</NavDropdown.Item>
    				        <NavDropdown.Divider />
    				        <NavDropdown.Item as={Link} to="/logout">log out</NavDropdown.Item>
    				      	</NavDropdown>
    				      	:
    				      	<Nav>
	    				      	<Nav.Link as={Link} to="/cart"><img src="cart.svg" style={{width: "20px"}} /><sup className="p-1 text-white bg-danger font-weight-bold">{cart.length}</sup></Nav.Link>


			      				<NavDropdown title={`signed in as: ${firstName}`} id="collasible-nav-dropdown">
			      	        	<NavDropdown.Item as={Link} to="/orderhistory">Order History</NavDropdown.Item>
			      	        	<NavDropdown.Item as={Link} to="/logout">log out</NavDropdown.Item>
			      	      		</NavDropdown>
			      	      	</Nav>

		    			}

		    			
		    	</Nav>
		    	:
		    	<Nav>
		          <Nav.Link as={Link} to="/register">Create New Account</Nav.Link>
		          <Nav.Link eventKey={2} as={Link} to="/login">Log In</Nav.Link>
		   	 	</Nav>

		    }


		    
		  </Navbar.Collapse>
		</Navbar>


		);

};