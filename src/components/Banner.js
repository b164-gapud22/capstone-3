import { Container } from 'react-bootstrap'




export default function Banner() {



	return(
		
			  <Container fluid className="py-md-5 px-md-5 text-light Banner">
			    <h1>QuickShop</h1>
			    <p>
			      Shop anywhere, anytime.
			    </p>
			  </Container>
		);
};