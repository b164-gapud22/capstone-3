import { useContext, useState, useEffect } from 'react';
import { Card, Col, Button, Row} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';



export default function ProductCard({productProp}) {


	const {user, archive} = useContext(UserContext);


	const {productName, description, price, _id, images} = productProp


	return(	

				(user.isAdmin) ?
				<Col>
					<Card style={{ width: '15rem' }} className=" text-left m-auto shadow ">
					  <Card.Img variant="top" src={images.url} />
					  <Card.Body>
					    <Card.Title>{productName}</Card.Title>
					    <Card.Text>{description}</Card.Text>
					    <Card.Text>&#8369;{price.toLocaleString(undefined, {maximumFractionDigits: 2})}</Card.Text>
					  </Card.Body>
					</Card>
				</Col>	
				:
				<Col>
					<Card style={{ width: '15rem' }} className="btn text-left m-auto shadow" as={Link} to={`/products/${_id}`} id="hover">
					  <Card.Img variant="top" src={images.url} />
					  <Card.Body>
					    <Card.Text>{productName}</Card.Text>
					    <Card.Title className="text-danger">&#8369;{price.toLocaleString(undefined, {maximumFractionDigits: 2})}</Card.Title>
					  </Card.Body>
					</Card>
				</Col>	

		)
}