
import { Container, Button } from 'react-bootstrap';
import {Fragment, useContext} from 'react';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';




export default function Cart() {
	
	const {cart, onAdd, onRemove, removeFromCart, removeAllCart} = useContext(UserContext);

	const itemsPrice = cart.reduce((a, c) => a + c.price * c.qty, 0)


	// Check out Items
	const checkout = (cart) => {

		cart.forEach(item => {

			const subtotal = item.qty * item.price;

			fetch('https://ecommerce-bymagic.herokuapp.com/marketplace/users/order', {
				method: "POST",
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem("token")}`
				},
				body: JSON.stringify({

					productId: item._id,
					productName: item.productName,
					description: item.description,
					price: item.price,
					stock: item.stock,
					images: item.images,
					quantity: item.qty,
					subtotal: subtotal
				})
			})
			.then(res => res.json())
			.then(data => {
				removeAllCart()
				if(data === true) {
					Swal.fire({
						title: "Thank You!!",
						icon: "success",
						text: "You have successfully checkout the item"
					})

				}else{
					Swal.fire({
						title: "Something went wrong!",
						icon: "error"
					})

				}
			})
		})

	} 









	return(
		
		<Container className="my-5 pb-4" id="Cart">
			<h3 className="text-center my-3 shadow border" id="header">Shopping Cart</h3>
			<div>
				<div>
					{cart.length === 0 && <div className="shadow p-4 d-block text-center" id="CartSummary">Cart is Empty</div>}
					{cart.map((item) => (
						<div key={cart._id} className="shadow p-4 my-2 border">
							<div className="d-flex flex-wrap">
								
									<div className="col-md-4">	
										<img src={item.images.url} alt={item.productName} style={{width: '15rem'}}/>
									</div>
									<div className="col-md-2">
									<h5>{item.productName}</h5>
									<p>{item.description}</p>

									<Button onClick={() => onRemove(item)} className="bg-danger my-auto">-</Button>
									<Button onClick={() => onAdd(item)} className="bg-info my-auto">+</Button>
									
									</div>
									<div className="col-md-2 text-right my-auto">{item.qty} x &#8369;{item.price.toLocaleString(undefined, {maximumFractionDigits: 2})}</div>
									<div className="col-md-2 text-right my-auto">&#8369;{(item.price * item.qty).toLocaleString(undefined, {maximumFractionDigits: 2})}</div>
							</div>
						</div>
						))}
					{cart.length !== 0 && (
						<>
							<hr></hr>
							<div className="text-right">
								<h3>Items Price</h3>
								<h3 className="text-danger">&#8369;{itemsPrice.toLocaleString(undefined, {maximumFractionDigits: 2})}</h3>
								<Button className="bg-info" onClick={() => checkout(cart)}>Checkout Items</Button>
								<br />
								<br />
								<Button className="bg-danger" onClick={() => removeAllCart()}>Clear Cart Items</Button>
							</div>
						</>
						)}
				</div>	
			</div>
		</Container>
			
		)
}	