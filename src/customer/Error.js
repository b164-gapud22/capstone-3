import { Container } from 'react-bootstrap';







export default function Error() {


	return(

			<Container className="text-center my-5 bg-light border">
				<img src="Error.jpg" />
				<h1 className="display-1 text-danger">404</h1>
				<h1 className="display-3 text-danger"> Ooops!!!!</h1>
				<h2>We can't seem to find the page you are looking for!</h2>
			</Container>



		);
};