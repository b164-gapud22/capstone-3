import { Row, Col, Button } from 'react-bootstrap';
import {Link} from 'react-router-dom';






export default function Home() {


	return(

		<Row className="container mx-auto my-3 py-3" id="home-box">
			<Col className="col-7 mx-auto">
				<img src="Shopper.png" className="img-fluid" controlId="shopper" />
			</Col>
			<Col className="my-auto">
				<Row>
					<Col className="pl-4">
						<h2><span className="text-danger">BIG</span> SAVINGS!!</h2>
						<h4>NEW ARRIVALS</h4>
						<Button className="bg-danger mr-2" as={Link} to="/login">Shop Now</Button>
						<Button className="bg-info d-default d-sm-inline-block" as={Link} to="/products">View Products</Button>
					</Col>
				</Row>
				<Row>
					<Col className="position-relative">
						<img src="shopping cart.png" className="img-fluid d-none d-sm-block" />
					</Col>
				</Row>
			</Col>
		</Row>



		);
};