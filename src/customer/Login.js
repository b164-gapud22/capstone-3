import { Form, Button } from 'react-bootstrap';
import {Link} from 'react-router-dom';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext'
import Swal from 'sweetalert2';
import { Navigate } from 'react-router-dom';


export default function Login() {

	// Unpack the data of the UserContext object
	const {user, setUser} = useContext(UserContext)


	// to store the value of the input fields
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	// to determine if the button is enabled
	const [isActive, setIsActive] = useState(false);



	// Authenticate and show alert for logging in.
	function authenticate(e) {

		// prevent the react from auto refresh
		e.preventDefault();




		fetch('https://ecommerce-bymagic.herokuapp.com/marketplace/users/login', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})	
		.then(res => res.json())
		.then(data => {
			

			if(typeof data.accessToken !== "undefined") {
				localStorage.setItem('token', data.accessToken)
				retrieveUserDetails(data.accessToken)

				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to Quickshop. Happy Shopping"
				})
			} else {
				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Check your login details and try again!"
				})
			}
		})


		setEmail("");
		setPassword("");

	}




	const retrieveUserDetails = (token) => {

		fetch('https://ecommerce-bymagic.herokuapp.com/marketplace/users/details', {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}




	//  to set condition to change the button to active if all requirements are met.
	useEffect(() => {


		if(email !== "" && password !== "") {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	})






	return(

			// the page will automatically redirect the user  to the products upon accessing this route "/login"

			(user.id !== null) ?

			<Navigate to="/" />

			:

			<div className="row container justify-content-around mx-auto my-5">
				<img src="shop.gif" className="col-lg-6 img-fluid" />

				<Form className="mt-5 p-lg-5 col-lg-6 shadow mb-4 bg-white rounded-lg" onSubmit={(e) => {authenticate(e)}}>
				  <Form.Group className="mb-3">
				    <Form.Label>Email address</Form.Label>
				    <Form.Control 
				    	type="email" 
				    	placeholder="Enter email"
				    	value={email}
				    	onChange={e => setEmail(e.target.value)}
				    	required
				    	autoComplete="on"
				    	/>
				  </Form.Group>

				  <Form.Group className="mb-3">
				    <Form.Label>Password</Form.Label>
				    <Form.Control 
				    	type="password" 
				    	placeholder="Password"
				    	value={password}
				    	onChange={e => setPassword(e.target.value)}
				    	/>
				  </Form.Group>


				  {
				  	isActive ?
				  	<Button variant="primary" type="submit" size="lg" className="btn-block">
				  	  Log In
				  	</Button>
				  	:
				  	<Button variant="danger" type="submit" size="lg" className="btn-block" disabled>
				  	  Log In
				  	</Button>
				  }


				  <Form.Text className="text-muted">
				        No account? <Link to="/register">Create one!</Link>
				      </Form.Text>

				</Form>
			</div>
		);
};