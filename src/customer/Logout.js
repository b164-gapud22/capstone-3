import { useContext, useEffect } from 'react'
import { Navigate } from 'react-router-dom';
// Redirect
import UserContext from '../UserContext'




export default function Logout() {

	const { unsetUser, setUser } = useContext(UserContext);

	// clear the localStorage
	unsetUser()


	useEffect(() => {
		setUser({id: null})
	}, [])


	return(

		<Navigate to="/" />

		)
}