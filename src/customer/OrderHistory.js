import { Table, Container } from 'react-bootstrap';
import { Fragment, useState, useEffect } from 'react';


export default function OrderHistory() {


const [sold, setSold] = useState([])


useEffect(() => {


	fetch('https://ecommerce-bymagic.herokuapp.com/marketplace/users/myOrder', {
		method: "GET",
		headers: {
			"Authorization": `Bearer ${localStorage.getItem("token")}`
		}
	})
	.then(res => res.json())
	.then(data => {

		setSold(data.cart)
	})
})





	return(

		<Container className="m-5 mx-auto text-center">	
		<h2 className="shadow border">Order History</h2>
			<Table responsive striped bordered hover  className="text-center" size="sm">
			  <thead>
			    <tr>
			      <th>Item</th>
			      <th>Name</th>
			      <th>Date</th>
			      <th>Price</th>
			      <th>Quantity</th>
			      <th>Total</th>
			    </tr>
			  </thead>
			  <tbody>
			  {sold.map((item) => 
			  	(
			    <tr key={sold._id}>
			      <td><img src={item.images[0].url} alt="" width="50"/></td>
			      <td>{item.productName}</td>
			      <td>{item.orderOn}</td>
			      <td>&#8369;{(item.price).toLocaleString(undefined, {maximumFractionDigits: 2})}</td>
			      <td>{item.quantity}</td>
			      <td>&#8369;{(item.subtotal).toLocaleString(undefined, {maximumFractionDigits: 2})}</td>
			    </tr>
			      )
			    )}
			  </tbody>
			</Table>
		</Container>

		)
}