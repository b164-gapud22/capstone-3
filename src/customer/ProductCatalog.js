import { Container, Form, InputGroup, Dropdown, DropdownButton, FormControl, Col, Row } from 'react-bootstrap';
import { Fragment, useState, useEffect} from 'react'
import ProductCard from '../components/ProductCard';



export default function Products() {

	const [products, setProduct] = useState([]);


	useEffect(() => {
		fetch('https://ecommerce-bymagic.herokuapp.com/marketplace/products/active')
		.then(res => res.json())
		.then(data => {

			setProduct(data.map(product => {
				return(
					<ProductCard key={product._id} productProp={product} />
					)
			}))

		})
	}, [])




	return(

		<Container className="my-5 bg-light py-5" fluid>
			<InputGroup className="mb-3 container">
			    <InputGroup.Text id="inputGroup-sizing-default">Search</InputGroup.Text>
			    <FormControl
			      aria-label="Default"
			      aria-describedby="inputGroup-sizing-default"
			    />
			  </InputGroup>

			<Row xs={1} md={3} lg={4} className="g-4 my-5">
				
					{products}
				
			</Row>
		</Container>


		)
}