import { useState, useEffect, useContext } from 'react';
import { useParams, Link  } from 'react-router-dom';
import { Container, Card, Button, Form } from 'react-bootstrap'
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { useNavigate } from 'react-router-dom';





export default function ProductVeiw() {

	const navigate =useNavigate();


	// unpack the userContext
	const {user, onAdd, product} = useContext(UserContext);


	// The useParams hook allows us to retrieve the courseId passed via the URL
	const {productId} = useParams()

	// Product Current Value
	const [productName, setProductName] = useState("");
	const [description, setDescription] = useState("");
	const [images, setImages] = useState("");
	const [price, setPrice] = useState(0);
	const [stock, setStock] = useState(0);
	const [item, setItem] = useState({});




	// add product to Cart
		const addtoCart = (productId) => {
			fetch('https://ecommerce-bymagic.herokuapp.com/marketplace/users/order', {
				method: "POST",
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem("token")}`
				},
				body: JSON.stringify({
					productId: productId
				})
			})
			.then(res => res.json())
			.then(data => {

				if(data === true) {
					Swal.fire({
						title: "Added to Cart",
						icon: "success",
						text: "You have successfully added the item to cart!"
					})
				}else{
					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again"
					})
				}
			})
		}






	// get the individual item.
	useEffect(() => {
		fetch(`https://ecommerce-bymagic.herokuapp.com/marketplace/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			setItem(data)
			setProductName(data.productName)
			setDescription(data.description)
			setImages(data.images)
			setStock(data.stock)
			setPrice(data.price)

		})
	}, [productId])


	// Update a product
	function updateProduct(e) {
		// prevent automatic refresh
		e.preventDefault()


		fetch(`https://ecommerce-bymagic.herokuapp.com/marketplace/products/${productId}/edit`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				productName: productName,
				description: description,
				price: price,
				stock: stock
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){

				// clear fields
				setProductName("");
				setDescription("");
				setStock("");
				setPrice("");

				Swal.fire({
					title: "You have successfully edited a product",
					icon: "success",
				})
				navigate("/allproducts");

			}else{
				Swal.fire({
					title: "Something went wrong!",
					icon: "error",
				})
			}
		})
	}






	return(

			(user.isAdmin) ?
			<Container className="justify-content-center my-4 d-flex flex-wrap">
				<div>
				<Card>
				  <Card.Img src={images.url} className="img-fluid" style={{ width: '25rem' }}/>
				  </Card>
				</div>
				<Card style={{ width: '25rem' }}>
				  	<div className="container px-4" >	
						  <Form onSubmit={(e) => updateProduct(e)}>
						  	<Form.Group>	
						    <Form.Label>Product Name:</Form.Label>
						    <Form.Control 
						    	type="text" 
						    	size="sm"
						    	placeholder={productName}
						    	value={productName}
						    	onChange={e => setProductName(e.target.value)}
						    	required
						    	 />
						    	
						 
						  	</Form.Group>

						  	<Form.Group>
						    <Form.Label>Description:</Form.Label>
						    <Form.Control 
						    	type="text" 
						    	size="sm"
						    	placeholder={description}
						    	value={description}
						    	onChange={e => setDescription(e.target.value)}
						    	required
						    	 />
						  	</Form.Group>

						  	<Form.Group>
						    <Form.Label>Price:</Form.Label>
						    <Form.Control 
						    	type="number" 
						    	size="sm"
						    	placeholder={price}
						    	min="1"
						    	value={price}
						    	onChange={e => setPrice(e.target.value)}
						    	required
						    	 />
						  	</Form.Group>

						  	<Form.Group>
						    <Form.Label>Stock:</Form.Label>
						    <Form.Control 
						    	type="number" 
						    	size="sm"
						    	placeholder={stock}
						    	min="1"
						    	value={stock}
						    	onChange={e => setStock(e.target.value)}
						    	required
						    	 />
						  	</Form.Group>

						  	<Button className="btn-block" type="submit">Update Product</Button>

						  </Form>
				  	</div>
				</Card>
			</Container>
			:
			<Container className="justify-content-center my-4 d-flex flex-wrap">
				<div>
				<Card className="shadow">
				  <Card.Img src={images.url} className="img-fluid" style={{ width: '25rem' }}/>
				  </Card>
				</div>
				<Card style={{ width: '25rem' }} className="shadow">
				  <Card.Body>
				    <Card.Title>{productName}</Card.Title>
				    <Card.Subtitle>Description:</Card.Subtitle>
				    <Card.Text>{description}</Card.Text>
				    <Card.Subtitle>Price:</Card.Subtitle>
				    <Card.Text className="text-danger">&#8369;{price.toLocaleString(undefined, {maximumFractionDigits: 2})}</Card.Text>
				    	

				    		{
				    			user.id !== null ?
				    			<div>
				    			<Button variant="info" className="mr-1" onClick={() => onAdd(item)} as={Link} to="/products">Add to Cart</Button>
				    			</div>
				    			:
				    			<div>
				    			<Button variant="info" className="mr-1" as={Link} to="/login">Log In to Shop</Button>
				    			</div>
				    		}

					    
				  </Card.Body>
				</Card>
			</Container>

		);
};