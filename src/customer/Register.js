import { Form, Button } from 'react-bootstrap';
import {Link} from 'react-router-dom';
import { useEffect, useState } from 'react';
import Swal from 'sweetalert2';
import { useNavigate } from 'react-router-dom';



export default function Register() {


	const navigate =useNavigate();

	// State hooks to store the value of the input fields
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [mobile, setMobile] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");
	// State to determine whether the button is enabled or not.
	const [isActive, setIsActive] = useState(false);



	function registerUser(e) {

		// prevent automatic refresh
		e.preventDefault()

		fetch('https://ecommerce-bymagic.herokuapp.com/marketplace/users/registration', {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobile: mobile,
				password: password1

			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true) {

				// Clear input fields
				setFirstName("");
				setLastName("");
				setMobile("");
				setEmail("");
				setPassword1("");
				setPassword2("");



				Swal.fire({
					title: "Account Successfully Created",
					icon: "success",
					text: "Welcome to Quickshop. Happy Shopping!"
				})
				navigate("/login")

			} else {
				Swal.fire({
					title: "Something went wrong!",
					icon: "error",
					text: "Email is already taken."
				})
			}
		})



	}





	useEffect(() => {

		if((firstName !== "" && lastName !== "" && email !== "" && mobile !== "" && password1 !== "" && password2 !== "") && (password1 === password2)) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [firstName, lastName, email, mobile, password1, password2])



	return(
			<div className="container row my-5 mx-auto d-flex">
				<img src="register.gif" className=" col-lg-6 py-5 img-fluid" />

				<Form className="col-lg-6 mt-5 shadow p-md-5 bg-white rounded-lg" onSubmit={(e) => {registerUser(e)}}>


				  <Form.Group className="mb-1" controlId="firstName">
				    <Form.Label>First Name:</Form.Label>
				    <Form.Control 
				    	type="text" 
				    	placeholder="Enter First Name" 
				    	value={firstName}
				    	onChange={e => setFirstName(e.target.value)}
				    	required
				    	/>
				  </Form.Group>

				  <Form.Group className="mb-1">
				    <Form.Label>Last Name:</Form.Label>
				    <Form.Control 
				    	type="text" 
				    	placeholder="Enter Last Name"
				    	value={lastName}
				    	onChange={e => setLastName(e.target.value)}
				    	required
				    	/>
				  </Form.Group>

				  <Form.Group className="mb-1">
				    <Form.Label>Email address:</Form.Label>
				    <Form.Control 
				    	type="email" 
				    	placeholder="Enter email"
				    	value={email}
				    	onChange={e => setEmail(e.target.value)}
				    	required
				    	/>
				  </Form.Group>

				  <Form.Group className="mb-1">
				    <Form.Label>Mobile Number:</Form.Label>
				    <Form.Control 
				    	type="tel" 
				    	placeholder="Enter Mobile Number"
				    	value={mobile}
				    	onChange={e => setMobile(e.target.value)}
				    	required
				    	/>
				  </Form.Group>

				  <Form.Group className="mb-1" controlId="password1">
				    <Form.Label>Password:</Form.Label>
				    <Form.Control 
				    	type="password" 
				    	placeholder="Enter Password"
				    	value={password1}
				    	onChange={e => setPassword1(e.target.value)}
				    	required
				    	/>
				  </Form.Group>

				  <Form.Group className="mb-1">
				    <Form.Label>Confirm Password:</Form.Label>
				    <Form.Control 
				    	type="password" 
				    	placeholder="Confirm Password"
				    	value={password2}
				    	onChange={e => setPassword2(e.target.value)}
				    	required
				    	/>
				  </Form.Group>

				  {
				  	isActive ?
				  	<Button variant="primary" type="submit" size="sm" className="btn-block mt-3">
				  	  Create New Account
				  	</Button>
				  	:
				  	<Button variant="danger" type="submit" size="sm" className="btn-block mt-3">
				  	  Create New Account
				  	</Button>
				  }

				  

				  <Form.Text className="text-muted">
				        Have an account already? <Link to="/login">Click to log in!</Link>
				      </Form.Text>

				</Form>
			</div>
		);
};